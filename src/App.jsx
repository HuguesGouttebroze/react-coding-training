import { useState, useEffect } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'

function App() {
  const [users, setUsers] = useState([]);
   useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/users")
    .then((response) => response.json())
    .then((json) => setUsers(json));
  }, []); 

  const mapData = () => {
    let filterArray = users.filter((user) => {
    return user.name.includes("a");
  })
  console.log(filterArray);
  setUsers(filterArray);
  }

  
  return (
    <div className="App">
      <h1>Users are using Vite + React</h1>
      <div className="card">
        {users.map((user) => (
          <div>
            <p>{user.name}</p>
            <p>{user.userName}</p>
            
          </div>
        ))}
      </div>
      <button onClick={mapData}>Multiply "user.id"</button>
    </div>
  )
}

export default App
